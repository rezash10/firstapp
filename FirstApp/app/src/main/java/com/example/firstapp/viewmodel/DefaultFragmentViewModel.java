package com.example.firstapp.viewmodel;

import static com.example.firstapp.viewmodel.Utils.TAG1;
import static com.example.firstapp.viewmodel.Utils.directoryName;


import android.app.Application;
import android.content.ContentValues;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioFormat;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.util.Patterns;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.example.firstapp.ActionListener;
import com.example.firstapp.view.adapters.DownloadListAdapter;
import com.tonyodev.fetch2.Download;
import com.tonyodev.fetch2.Error;
import com.tonyodev.fetch2.Fetch;
import com.tonyodev.fetch2.FetchConfiguration;
import com.tonyodev.fetch2.FetchListener;
import com.tonyodev.fetch2.NetworkType;
import com.tonyodev.fetch2.Priority;
import com.tonyodev.fetch2.Request;
import com.tonyodev.fetch2core.DownloadBlock;
import com.tonyodev.fetch2core.Downloader;
import com.tonyodev.fetch2core.Extras;
import com.tonyodev.fetch2core.Func;
import com.tonyodev.fetch2okhttp.OkHttpDownloader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

public class DefaultFragmentViewModel extends AndroidViewModel implements ActionListener {

    //---------------Variables-------------//
    public MutableLiveData<Boolean> checkLink = new MutableLiveData<>();
    public MutableLiveData<Boolean> fileExists = new MutableLiveData<>();
    private final Application applicationContext;
    Fetch fetch;
    public DownloadListAdapter adapter;
    public static boolean pauseAll = false;
    public static boolean cancelAll = false;


    //-----------Constructor-----------//
    public DefaultFragmentViewModel(@NonNull Application application) {
        super(application);
        applicationContext = application;
        checkLink.setValue(true);
        adapter = new DownloadListAdapter(this, application);




    }

    private OkHttpDownloader getOkHttpDownloader() {

        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .followRedirects(true)
                .followSslRedirects(true)
                .connectTimeout(10000,TimeUnit.MILLISECONDS)
                .build();
        return new OkHttpDownloader(okHttpClient, Downloader.FileDownloaderType.PARALLEL);
    }


    public void downloadLink(String link, String fileName) {

                Log.i(TAG1+"viewholder", Thread.currentThread().getName());


                boolean isLinkValid = Patterns.WEB_URL.matcher(link).matches();


                if (isLinkValid) {

                    //------Preparing file name and mime-----------//

                    if (fileName.isEmpty()) {
                        fileName = "File" + UUID.randomUUID();
                    }

                    String extension = MimeTypeMap.getFileExtensionFromUrl(link);

                    if (!extension.isEmpty()) {
                        fileName = fileName + "." + extension;
                    }


                    //---------downloading file---------//

                    String uri;
                    if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.Q){
                        uri = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath() + "/" + fileName;

                    }else{
                        Log.i(TAG1, "downloadLink: ");
                        File path = new File(Environment.getExternalStorageDirectory(), directoryName);
                        if(!path.exists()){
                            path.mkdir();
                        }
                        File file = new File(path,fileName);
                        uri = file.getAbsolutePath();
                        Log.i(TAG1, uri);


                    }





                    String finalFileName = fileName;


                    Map<String, String> titleMap = new HashMap<>();
                    titleMap.put("title", finalFileName);

                    Request request = new Request(link, uri);
                    request.setPriority(Priority.HIGH);
                    request.setNetworkType(NetworkType.ALL);
                    request.setExtras(new Extras(titleMap));


                    fetch.enqueue(request, new Func<Request>() {
                        @Override
                        public void call(@NonNull Request result) {
                        }
                    }, new Func<Error>() {
                        @Override
                        public void call(@NonNull Error result) {

                            if (result.getValue() == Error.FILE_NOT_FOUND.getValue()) {
                                Toast.makeText(applicationContext, "نام فایل تکراری است", Toast.LENGTH_SHORT).show();
                            }else{
                                Toast.makeText(applicationContext, "خطای رخ داده است", Toast.LENGTH_SHORT).show();

                            }
                        }
                    }).addListener(fetchListener);





                } else {
                    checkLink.setValue(false);
                }


    }




    FetchListener fetchListener = new FetchListener() {
        @Override
        public void onAdded(@NonNull Download download) {
            if (fileExists.getValue().equals(false)) {
                fileExists.setValue(true);
            }
            adapter.addDownload(download);

        }

        @Override
        public void onCancelled(@NonNull Download download) {
            adapter.update(download);

        }

        @Override
        public void onCompleted(@NonNull Download download) {
            adapter.update(download);


        }

        @Override
        public void onDeleted(@NonNull Download download) {
            adapter.update(download);

        }

        @Override
        public void onDownloadBlockUpdated(@NonNull Download download, @NonNull DownloadBlock downloadBlock, int totalBlocks) {

        }

        @Override
        public void onError(@NonNull Download download, @NonNull Error error, @Nullable Throwable throwable) {
            adapter.update(download);

        }

        @Override
        public void onPaused(@NonNull Download download) {
            adapter.update(download);


        }

        @Override
        public void onProgress(@NonNull Download download, long etaInMilliSeconds, long downloadedBytesPerSecond) {
            adapter.update(download);


        }

        @Override
        public void onQueued(@NonNull Download download, boolean waitingOnNetwork) {
            adapter.update(download);



        }

        @Override
        public void onRemoved(@NonNull Download download) {

            adapter.update(download);


        }

        @Override
        public void onResumed(@NonNull Download download) {

            adapter.update(download);


        }

        @Override
        public void onStarted(@NonNull Download download, @NonNull List<? extends DownloadBlock> downloadBlocks, int totalBlocks) {


        }

        @Override
        public void onWaitingNetwork(@NonNull Download download) {

        }
    };

    public void onResume() {




                FetchConfiguration configuration = new FetchConfiguration.Builder(applicationContext)
                        .enableRetryOnNetworkGain(true)
                        .enableLogging(true)
                        .setDownloadConcurrentLimit(3)
                        .setHttpDownloader(getOkHttpDownloader())
                        .build();


                fetch = Fetch.Impl.getInstance(configuration);



                fetch.getDownloads(new Func<List<Download>>() {
                    @Override
                    public void call(@NonNull List<Download> result) {

                        Collections.sort(result,(first,second)->Long.compare(first.getCreated(),second.getCreated()));

                        for (Download res : result) {

                            adapter.addDownload(res);


                        }

                        if (result.size() > 0) {
                            fileExists.setValue(true);
                        } else {
                            fileExists.setValue(false);
                        }



                    }
                }).addListener(fetchListener);




    }


    public void onPause() {
        fetch.removeListener(fetchListener);
    }

    public void onDestroy() {
        fetch.close();
    }

    @Override
    public void onPauseDownload(int id) {
        fetch.pause(id);
    }

    @Override
    public void onRemoveDownload(int id) {
        fetch.remove(id);
    }

    @Override
    public void onRetryDownload(int id) {
        fetch.retry(id);
    }

    @Override
    public void onResumeDownload(int id) {
        fetch.resume(id);
    }

}



