package com.example.firstapp.view.fragments;

import static com.example.firstapp.viewmodel.Utils.TAG1;
import static com.example.firstapp.viewmodel.Utils.TAG2;

import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.view.ViewGroupCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;

import com.example.firstapp.R;
import com.example.firstapp.databinding.DialogFileNameInputBinding;
import com.example.firstapp.databinding.FragmentDefaultBinding;
import com.example.firstapp.viewmodel.DefaultFragmentViewModel;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import com.google.android.material.transition.MaterialSharedAxis;


public class DefaultFragment extends Fragment {

    FragmentDefaultBinding binding;
    DefaultFragmentViewModel viewModel;
    String link;
    Fragment currentFragment;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentDefaultBinding.inflate(inflater);

        viewModel =
                new ViewModelProvider(this).get(DefaultFragmentViewModel.class);

        ViewGroupCompat.setTransitionGroup(binding.defaultViewGroup,true);


        binding.setViewModel(viewModel);
        binding.setLifecycleOwner(getViewLifecycleOwner());

        binding.rvDownloadList.setAdapter(viewModel.adapter);

        currentFragment = requireActivity().getSupportFragmentManager().findFragmentById(R.id.fragment)
                .getChildFragmentManager().getFragments().get(0);

        Log.i(TAG1, currentFragment.toString());


        TextInputLayout etLink = (TextInputLayout) binding.etLink;
        ImageButton btnDownload = binding.ibDownloadLink;


        etLink.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (etLink.getError() != null) {
                    etLink.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        btnDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                InputMethodManager manager = (InputMethodManager) requireContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                manager.hideSoftInputFromWindow(view.getWindowToken(), 0);

                if (getConnection()) {

                    link = etLink.getEditText().getText().toString().trim();
                    if (!link.isEmpty()) {

                        showFileNameDialog(link);


                    } else {
                        etLink.setError("لینک");
                        Snackbar.make(binding.getRoot(), "لینک را وارد کنید ", Snackbar.LENGTH_SHORT).show();
                    }


                } else {
                    Snackbar.make(binding.getRoot(), "اینترنت گوشی خود را چک کنید ", Snackbar.LENGTH_INDEFINITE).
                            setAction("لغو", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                }
                            }).show();

                }


            }
        });


        viewModel.checkLink.observe(requireActivity(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if (!aBoolean) {
                    Snackbar.make(binding.getRoot(), "لینک قابل دانلود نیست", Snackbar.LENGTH_LONG).show();
                }
            }
        });

        viewModel.fileExists.observe(requireActivity(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                binding.pbLoadingLastList.setVisibility(View.GONE);
                if (aBoolean) {
                    binding.rvDownloadList.setVisibility(View.VISIBLE);
                    binding.ivNotFound.setVisibility(View.GONE);
                    binding.txNotFound.setVisibility(View.GONE);
                } else {
                    binding.ivNotFound.setVisibility(View.VISIBLE);
                    binding.txNotFound.setVisibility(View.VISIBLE);
                }
            }
        });


        setHasOptionsMenu(true);

        return binding.getRoot();

    }


    private boolean getConnection() {
        ConnectivityManager manager = (ConnectivityManager) requireContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED
                || manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            return true;
        }
        return false;
    }

    private void showFileNameDialog(String link) {

        DialogFileNameInputBinding dialogBinding = DialogFileNameInputBinding.inflate(getLayoutInflater());
        new MaterialAlertDialogBuilder(requireActivity(), R.style.MaterialCustomAlertDialog)
                .setTitle("نام فایل")
                .setView(dialogBinding.getRoot())
                .setNegativeButton("لغو", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                }).setPositiveButton("دانلود", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {



                            Log.i(TAG2 + "fragment", Thread.currentThread().getName());
                        String fileName = dialogBinding.etFileName.getEditText().getText().toString();
                        viewModel.downloadLink(link,fileName);


                     ;





            }
        }).setCancelable(true)
                .show();
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.drawer_menu_items, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.full_list) {
            currentFragment.setExitTransition(new MaterialSharedAxis(MaterialSharedAxis.Z,true)
            .setDuration(1000))
            ;
            currentFragment.setReenterTransition(new MaterialSharedAxis(MaterialSharedAxis.Z,false)
            .setDuration(1000));
            Navigation.findNavController(requireActivity(), R.id.fragment)
                    .navigate(R.id.action_defaultFragment_to_fullListFragment);


        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        Log.i(TAG1, "onStart: ");
        super.onStart();

        viewModel.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();
        if(viewModel!=null){
            viewModel.onPause();

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(viewModel!=null){
            viewModel.onDestroy();

        }
    }
}


