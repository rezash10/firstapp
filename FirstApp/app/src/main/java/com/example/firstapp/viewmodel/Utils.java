package com.example.firstapp.viewmodel;

import android.app.Application;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.format.DateFormat;
import android.util.Log;
import android.webkit.MimeTypeMap;

import androidx.annotation.RequiresApi;

import com.example.firstapp.view.models.DownloadModel;
import com.example.firstapp.view.models.FileInfoModel;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

public class Utils {

    //----------variables---------------//
    static public String TAG1 = "GLOBAL_TAG";
    static public String TAG2 = "GLOBAL_TAG_INT";
    static public String CHANNEL_ID = "DOWNLOAD_CHANNEL";
    static protected final String APPLICATION_MIME = "application";
    static protected final String IMAGE_MIME = "image";
    static protected final String VIDEO_MIME = "video";
    static protected final String AUDIO_MIME = "audio";
    static private List<FileInfoModel> filesList;
    static protected String directoryName = "FirstAppDownload";

    public static List<FileInfoModel> getFilesList() {
        return filesList;
    }

    private static String filePath;
    static public String RESUME = "RESUME";
    static public String PAUSE = "PAUSE";


    //---------getting files for lower api--------------//



    static protected List<FileInfoModel> getAllFilesFromLowerAPi(Application application) {

        filesList = new ArrayList<>();



        File file = new File(Environment.getExternalStorageDirectory(),directoryName);
        if(file.exists()){
            for(File f:file.listFiles()){

                String volume = convertFileSize(f.length());
                String date = DateFormat.format("yyyy/MM/dd", f.lastModified() ).toString();

                String extension = MimeTypeMap.getFileExtensionFromUrl(f.getAbsolutePath());
                String mime = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);

                FileInfoModel fileModel = new FileInfoModel(f.getAbsolutePath(),volume,f.getName(),date,1,mime);

                filesList.add(fileModel);

            }

        }

        return filesList;

    }




    @RequiresApi(api = Build.VERSION_CODES.Q)
    static protected List<FileInfoModel> getAllFilesFromHigherAPi(Application application){

        filesList = new ArrayList<>();


        Uri uri = MediaStore.Files.getContentUri("external");







        Cursor cursor = application.getContentResolver().query(uri, null,null
                , null, null);


        if (cursor.moveToFirst()) {

            do {
                String mimeTypes = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.MIME_TYPE));
                String[] mimeParts = mimeTypes.split("/");

                switch (mimeParts[0]) {
                    case APPLICATION_MIME: {
                        getApplicationInfo(cursor, mimeTypes);
                        break;
                    }
                    case IMAGE_MIME: {
                        getImageInfo(cursor, mimeTypes);
                        break;

                    }
                    case VIDEO_MIME: {
                        getVideoInfo(cursor, mimeTypes);
                        break;

                    }
                    case AUDIO_MIME: {
                        getAudioInfo(cursor, mimeTypes);
                        break;
                    }
                }
            } while (cursor.moveToNext());

            cursor.close();




        }

        return filesList;
    }






    static public void getAudioInfo(Cursor cursor, String mimeType) {
        int fileNameIndex = cursor.getColumnIndex(MediaStore.Audio.Media.DISPLAY_NAME);
        int fileSizeIndex = cursor.getColumnIndex(MediaStore.Audio.Media.SIZE);
        int filePathIndex = cursor.getColumnIndex(MediaStore.Audio.Media.DATA);
        int fileTimeAddedIndex = cursor.getColumnIndex(MediaStore.Audio.Media.DATE_ADDED);
        int fileIdIndex = cursor.getColumnIndex(MediaStore.Audio.Media._ID);


        addFileInfoToList(cursor, fileNameIndex, filePathIndex, fileSizeIndex, fileTimeAddedIndex, fileIdIndex, mimeType);

    }


    //-----------Video Mime Info---------------//

    static public void getVideoInfo(Cursor cursor, String mimeType) {
        int fileNameIndex = cursor.getColumnIndex(MediaStore.Video.Media.DISPLAY_NAME);
        int fileSizeIndex = cursor.getColumnIndex(MediaStore.Video.Media.SIZE);
        int filePathIndex = cursor.getColumnIndex(MediaStore.Video.Media.DATA);
        int fileTimeAddedIndex = cursor.getColumnIndex(MediaStore.Video.Media.DATE_ADDED);
        int fileIdIndex = cursor.getColumnIndex(MediaStore.Video.Media._ID);


        addFileInfoToList(cursor, fileNameIndex, filePathIndex, fileSizeIndex, fileTimeAddedIndex, fileIdIndex, mimeType);
    }

    //------------Image Mime Info-------------//

    static public void getImageInfo(Cursor cursor, String mimeType) {
        int fileNameIndex = cursor.getColumnIndex(MediaStore.Images.Media.DISPLAY_NAME);
        int fileSizeIndex = cursor.getColumnIndex(MediaStore.Images.Media.SIZE);
        int filePathIndex = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
        int fileTimeAddedIndex = cursor.getColumnIndex(MediaStore.Images.Media.DATE_ADDED);
        int fileIdIndex = cursor.getColumnIndex(MediaStore.Images.Media._ID);


        addFileInfoToList(cursor, fileNameIndex, filePathIndex, fileSizeIndex, fileTimeAddedIndex, fileIdIndex, mimeType);


    }

    //----------Application Mime Infos-----------//
    static public void getApplicationInfo(Cursor cursor, String mimeType) {

        int filePathIndex = cursor.getColumnIndex(MediaStore.DownloadColumns.DATA);
        int fileSizeIndex = cursor.getColumnIndex(MediaStore.DownloadColumns.SIZE);
        int fileNameIndex = cursor.getColumnIndex(MediaStore.DownloadColumns.DISPLAY_NAME);
        int fileAddedTimeIndex = cursor.getColumnIndex(MediaStore.DownloadColumns.DATE_ADDED);
        int fileIdIndex = cursor.getColumnIndex(MediaStore.DownloadColumns._ID);



        addFileInfoToList(cursor, fileNameIndex, filePathIndex, fileSizeIndex, fileAddedTimeIndex, fileIdIndex, mimeType);


    }

    //-----------Add Files Info To List----------//
    static private void addFileInfoToList(Cursor cursor, int fileNameIndex, int filePathIndex, int fileSizeIndex
            , int fileTimeAddedIndex, int fileIdIndex, String mime)
    {

        String fileName = cursor.getString(fileNameIndex);

        if(fileName==null||fileName.isEmpty()){
            int fileTitleIndex = cursor.getColumnIndex(MediaStore.DownloadColumns.TITLE);
            fileName = cursor.getString(fileTitleIndex);
        }



        filePath = cursor.getString(filePathIndex);
        long id = cursor.getLong(fileIdIndex);


        String fileSize = cursor.getString(fileSizeIndex);

        if(fileSize==null){
            File file = new File(filePath);
            fileSize = String.valueOf(file.length());
        }



        float fileSizeInKB = Float.parseFloat(fileSize);
        String fileFormattedSize = convertFileSize(fileSizeInKB);



        String fileAddedTime = cursor.getString(fileTimeAddedIndex);
        String date = DateFormat.format("yyyy/MM/dd", Long.parseLong(fileAddedTime) * 1000).toString();

        FileInfoModel model = new FileInfoModel(filePath, fileFormattedSize, fileName, date, id, mime);
        filesList.add(model);
    }

    //------------Converting B to Proper KB,MB,TB size-------------//
    static public String convertFileSize(float fileSize) {
        double size = fileSize / Math.pow(2, 20);
        String sizeRes;
        if (size >= 1) {

            if (size >= 1000) {
                //Handling GB size
                size = size / Math.pow(2, 10);
                sizeRes = String.format("%.2f TB", size);

            }

            //Handling MB size

            sizeRes = String.format("%.2f MB", size);


        } else {
            //Handling KB size
            size = fileSize / Math.pow(2, 10);
            sizeRes = String.format("%.2f KB", size);

        }

        return sizeRes;
    }

    static protected void makeNewFileListObject(){
        filesList = new ArrayList<>();
    }





}
