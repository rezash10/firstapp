package com.example.firstapp.viewmodel;

import static com.example.firstapp.viewmodel.Utils.APPLICATION_MIME;
import static com.example.firstapp.viewmodel.Utils.TAG1;
import static com.example.firstapp.viewmodel.Utils.convertFileSize;
import static com.example.firstapp.viewmodel.Utils.directoryName;
import static com.example.firstapp.viewmodel.Utils.getAllFilesFromHigherAPi;
import static com.example.firstapp.viewmodel.Utils.getAllFilesFromLowerAPi;
import static com.example.firstapp.viewmodel.Utils.getAudioInfo;
import static com.example.firstapp.viewmodel.Utils.getFilesList;
import static com.example.firstapp.viewmodel.Utils.getImageInfo;

import android.app.Application;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.telephony.mbms.FileInfo;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.firstapp.view.models.FileInfoModel;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FullFragmentViewModel extends AndroidViewModel {

    //---------Variables------------//
    private Application application;
    private List<FileInfoModel> fileInfoList;
    public MutableLiveData<Boolean> fileExists = new MutableLiveData<>();
    private MutableLiveData<List<FileInfoModel>> _fileListLiveData = new MutableLiveData<>();
    Handler handler = new Handler(Looper.getMainLooper());
    private View viewHodler;
    public LiveData<List<FileInfoModel>> getFileListLiveData() {
        return _fileListLiveData;
    }



    public FullFragmentViewModel(@NonNull Application application) {
        super(application);
        this.application = application;

        retrieveFilesFromDownloads();
    }

    //--------Retrieving files from directory----------/
    private void retrieveFilesFromDownloads() {

        new Thread(new Runnable() {
            @Override
            public void run() {


                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {


                    fileInfoList = getAllFilesFromHigherAPi(application);


                    handler.post(new Runnable() {
                        @Override
                        public void run() {

                            if (fileInfoList.size() > 0) {

                                fileExists.setValue(true);
                                _fileListLiveData.setValue(fileInfoList);

                            } else {

                                fileExists.setValue(false);

                            }
                        }
                    });


                } else {

                    Log.i(TAG1, "Retrieve");
                    fileInfoList = getAllFilesFromLowerAPi(application);

                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (fileInfoList.size() > 0) {

                                fileExists.setValue(true);
                                _fileListLiveData.setValue(fileInfoList);

                            } else {

                                fileExists.setValue(false);

                            }
                        }
                    });
                }

            }
        }).start();

    }

    //-----------retrieve musics --------------//
    public void onMusicFilterClicked(View view){
        if (viewHodler!=null){
            viewHodler.setSelected(false);
            if(viewHodler==view){
                retrieveFilesFromDownloads();
                viewHodler = null;
                return;
            }
        }

        view.setSelected(true);
        viewHodler = view;


        new Thread(new Runnable() {
            @Override
            public void run() {

                Utils.makeNewFileListObject();

                Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;

                String selection = null;


                if(Build.VERSION.SDK_INT<Build.VERSION_CODES.Q){
                    File path = new File(Environment.getExternalStorageDirectory(),directoryName);

                    //select all media files in our app directory

                    selection = MediaStore.Audio.Media.DATA + " like " + "'%" + path.toString()+"/%'";

                }

                Cursor musicCursor = application.getContentResolver().query(uri,null,selection,null,null);

                if(musicCursor.moveToFirst()){
                    do {
                        String mime = musicCursor.getString(musicCursor.getColumnIndex(MediaStore.Audio.Media.MIME_TYPE));
                        getAudioInfo(musicCursor,mime);
                    }while (musicCursor.moveToNext());
                }

                fileInfoList = getFilesList();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (fileInfoList.size() > 0) {
                            _fileListLiveData.setValue(fileInfoList);
                            fileExists.setValue(true);
                        } else {
                            fileExists.setValue(false);
                        }
                    }
                });


            }
        }).start();

    }



    //------------retrieve videos---------------//
    public void onVideoFilterClicked(View view){

        if (viewHodler!=null){
            viewHodler.setSelected(false);
            if(viewHodler==view){
                retrieveFilesFromDownloads();
                viewHodler = null;
                return;
            }
        }

        view.setSelected(true);
        viewHodler = view;


        new Thread(new Runnable() {
            @Override
            public void run() {

                Utils.makeNewFileListObject();

                Uri uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;



                if(Build.VERSION.SDK_INT<Build.VERSION_CODES.Q){
                    File path = new File(Environment.getExternalStorageDirectory(),directoryName);
                    if(path.exists()){
                        List<FileInfoModel> filesList = new ArrayList<>();
                        for (File f:path.listFiles()) {

                            String extension = MimeTypeMap.getFileExtensionFromUrl(f.getAbsolutePath());
                            String mime = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);


                            if(mime.split("/")[0].equals("video")){
                                String volume = convertFileSize(f.length());

                                String date = DateFormat.format("yyyy/MM/dd", f.lastModified()).toString();

                                FileInfoModel fileModel = new FileInfoModel(f.getAbsolutePath(),volume,f.getName(),date,1,mime);

                                filesList.add(fileModel);

                            }

                        }

                        handler.post(new Runnable() {
                            @Override
                            public void run() {

                                if (filesList.size() > 0) {
                                    fileExists.setValue(true);
                                    _fileListLiveData.setValue(filesList);

                                } else {
                                    fileExists.setValue(false);
                                }
                            }
                        });


                    }

                }else{
                    Cursor musicCursor = application.getContentResolver().query(uri,null,null,null,null);

                    if(musicCursor.moveToFirst()){
                        do {
                            String mime = musicCursor.getString(musicCursor.getColumnIndex(MediaStore.Video.Media.MIME_TYPE));
                            Utils.getVideoInfo(musicCursor,mime);
                        }while (musicCursor.moveToNext());
                    }

                    fileInfoList = getFilesList();
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (fileInfoList.size() > 0) {
                                fileExists.setValue(true);
                                _fileListLiveData.setValue(fileInfoList);

                            } else {
                                fileExists.setValue(false);
                            }
                        }
                    });
                }




            }
        }).start();

    }


    //------------retrieve images---------------//
    public void onImageFilterClicked(View view){


        if (viewHodler!=null){
            viewHodler.setSelected(false);
            if(viewHodler==view){
                retrieveFilesFromDownloads();
                viewHodler = null;
                return;
            }
        }

        view.setSelected(true);
        viewHodler = view;


        new Thread(new Runnable() {
            @Override
            public void run() {

                Utils.makeNewFileListObject();

                Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;




                if(Build.VERSION.SDK_INT<Build.VERSION_CODES.Q){
                    File path = new File(Environment.getExternalStorageDirectory(),directoryName);
                    if(path.exists()){
                        List<FileInfoModel> filesList = new ArrayList<>();
                        for (File f:path.listFiles()) {

                            String extension = MimeTypeMap.getFileExtensionFromUrl(f.getAbsolutePath());
                            String mime = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
                            Log.i(TAG1, mime);

                            if(mime.split("/")[0].equals("image")){
                                String volume = convertFileSize(f.length());

                                String date = DateFormat.format("yyyy/MM/dd", f.lastModified()).toString();

                                FileInfoModel fileModel = new FileInfoModel(f.getAbsolutePath(),volume,f.getName(),date,1,mime);

                                filesList.add(fileModel);

                            }

                        }

                        handler.post(new Runnable() {
                            @Override
                            public void run() {

                                if (filesList.size() > 0) {
                                    fileExists.setValue(true);
                                    _fileListLiveData.setValue(filesList);

                                } else {
                                    fileExists.setValue(false);
                                }
                            }
                        });


                    }

                }else{

                    Cursor musicCursor = application.getContentResolver().query(uri,null,null,null,null);

                    if(musicCursor.moveToFirst()){
                        do {
                            String mime = musicCursor.getString(musicCursor.getColumnIndex(MediaStore.Images.Media.MIME_TYPE));
                            getImageInfo(musicCursor,mime);
                        }while (musicCursor.moveToNext());
                    }

                    fileInfoList = getFilesList();
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (fileInfoList.size() > 0) {
                                fileExists.setValue(true);
                                _fileListLiveData.setValue(fileInfoList);

                            } else {
                                fileExists.setValue(false);
                            }

                        }
                    });
                }




            }
        }).start();

    }

    //------------retrieve others---------------//
    public void onOtherFilterClicked(View view){

        if (viewHodler!=null){
            viewHodler.setSelected(false);
            if(viewHodler==view){
                retrieveFilesFromDownloads();
                viewHodler = null;
                return;
            }
        }

        view.setSelected(true);
        viewHodler= view;
        new Thread(new Runnable() {
            @Override
            public void run() {

                Utils.makeNewFileListObject();




                if(Build.VERSION.SDK_INT<Build.VERSION_CODES.Q){
                    File path = new File(Environment.getExternalStorageDirectory(),directoryName);
                    if(path.exists()){
                        List<FileInfoModel> filesList = new ArrayList<>();
                        for (File f:path.listFiles()) {

                            String extension = MimeTypeMap.getFileExtensionFromUrl(f.getAbsolutePath());
                            String mime = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);

                            if(mime.split("/")[0].equals(APPLICATION_MIME)){
                                String volume = convertFileSize(f.length());

                                String date = DateFormat.format("yyyy/MM/dd", f.lastModified() ).toString();

                                FileInfoModel fileModel = new FileInfoModel(f.getAbsolutePath(),volume,f.getName(),date,1,mime);

                                filesList.add(fileModel);

                            }

                        }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {

                                    if (filesList.size() > 0) {
                                        fileExists.setValue(true);
                                        _fileListLiveData.setValue(filesList);

                                    } else {
                                        fileExists.setValue(false);
                                    }
                                }
                            });


                    }

                }
                else{
                    Uri uri = MediaStore.Files.getContentUri("external");
                    Cursor musicCursor = application.getContentResolver().query(uri,null,null,null,null);

                    if(musicCursor.moveToFirst()){
                        do {
                            String mimeType = musicCursor.getString(musicCursor.getColumnIndex(MediaStore.Audio.Media.MIME_TYPE));
                            String mime = mimeType.split("/")[0];
                            if(mime.equals(APPLICATION_MIME)){
                                Utils.getApplicationInfo(musicCursor,mimeType);
                            }

                        }while (musicCursor.moveToNext());
                    }

                    fileInfoList = getFilesList();
                    handler.post(new Runnable() {
                        @Override
                        public void run() {

                            if (fileInfoList.size() > 0) {
                                fileExists.setValue(true);
                                _fileListLiveData.setValue(fileInfoList);

                            } else {
                                fileExists.setValue(false);
                            }
                        }
                    });
                }





            }
        }).start();

    }


}
