package com.example.firstapp.view.adapters;

import static com.example.firstapp.viewmodel.Utils.TAG1;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.example.firstapp.databinding.ModelFileBinding;
import com.example.firstapp.view.models.FileInfoModel;

import java.io.File;

public class FilesAdapter extends ListAdapter<FileInfoModel, FilesAdapter.ViewHolder> {



    public FilesAdapter() {

        super(new DiffUtils());

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ModelFileBinding binding = ModelFileBinding.inflate(inflater, parent, false);

        return new ViewHolder(binding);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.insertData(getItem(position));


        holder.constraintView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //-------change color of background when selected--------//
                view.setSelected(true);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        view.setSelected(false);
                    }
                }, 100);

                //----------opening documents------------//
                openDocument(holder, view);


            }
        });
    }

    private void openDocument(ViewHolder holder, View view) {

        Uri fileUri;
        String mimeType = getItem(holder.getAdapterPosition()).getFileMime();
        String mime = mimeType.split("/")[1];


        if (!mime.equals("octet-stream")) {



            Intent intent = new Intent(Intent.ACTION_VIEW);


            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);


            fileUri = FileProvider.getUriForFile(view.getContext(), view.getContext().getPackageName()
                    + ".provider", new File(getItem(holder.getAdapterPosition()).getFilePath()));


            intent.setData(fileUri);




            try{
                view.getContext().startActivity(intent);
            }catch(Exception e){
                Toast.makeText(view.getContext(), "برنامه ای برای باز کردن این فایل پیدا نشد.", Toast.LENGTH_LONG).show();
            }

        } else {

            Toast.makeText(view.getContext(), "برنامه ای برای باز کردن این فایل پیدا نشد.", Toast.LENGTH_LONG).show();

        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private ModelFileBinding binding;
        public CardView cardView;
        public ConstraintLayout constraintView;

        public ViewHolder(@NonNull ModelFileBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            this.constraintView = binding.constraintL;

        }

        public void insertData(FileInfoModel model) {

            binding.setFileModel(model);
            binding.executePendingBindings();
        }


    }
}
