package com.example.firstapp.view.adapters;

import android.annotation.SuppressLint;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;

import com.example.firstapp.view.models.FileInfoModel;

public class DiffUtils extends DiffUtil.ItemCallback<FileInfoModel> {


    @Override
    public boolean areItemsTheSame(@NonNull FileInfoModel oldItem, @NonNull FileInfoModel newItem) {
        return oldItem.getFileName()==newItem.getFileName();

    }

    @SuppressLint("DiffUtilEquals")
    @Override
    public boolean areContentsTheSame(@NonNull FileInfoModel oldItem, @NonNull FileInfoModel newItem) {
        return oldItem==newItem;
    }
}
