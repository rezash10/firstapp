package com.example.firstapp.view.adapters;

import static com.example.firstapp.viewmodel.Utils.TAG2;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import com.example.firstapp.R;

import com.example.firstapp.view.models.FileInfoModel;
import com.google.android.material.progressindicator.LinearProgressIndicator;
import com.tonyodev.fetch2.Status;


import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class DataBinadingUtils {



    @BindingAdapter("setupFullRecyclerView")
    static public void setupFullRecyclerView(RecyclerView rv, List<FileInfoModel> itemsList) {


        if (itemsList != null && itemsList.size() > 0) {

            FilesAdapter adapter = new FilesAdapter();
            rv.setAdapter(adapter);
            adapter.submitList(itemsList);
        }

    }


    @BindingAdapter("setImage")
    static public void setImage(CircleImageView imageView, String filePath) {


        //-------------setup image from filePath-------------//

        Glide.with(imageView.getContext()).load(R.drawable.civ_document).into(imageView);

//        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
//
//        try {
//            retriever.setDataSource(imageView.getContext(), Uri.parse(filePath));
//            byte[] imageBytes = retriever.getEmbeddedPicture();
//            if (imageBytes != null && imageBytes.length > 0) {
//
//                Bitmap bitmap = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
//                imageView.setImageBitmap(bitmap);
//
//            } else {
//                Glide.with(imageView.getContext()).load(filePath).placeholder(R.drawable.civ_document).into(imageView);
//            }
//
//        } catch (Exception e) {
//            Glide.with(imageView.getContext()).load(filePath).placeholder(R.drawable.civ_document).into(imageView);
//
//        } finally {
//            retriever.release();
//        }




    }



}
