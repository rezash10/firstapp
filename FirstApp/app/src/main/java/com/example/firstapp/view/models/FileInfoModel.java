package com.example.firstapp.view.models;

public class FileInfoModel {

    final private String filePath;
    final private String size;
    final private String fileName;
    final private String fileAddedTime;
    final private long fileId;
    final private String fileMime;



    public FileInfoModel(String filePath, String size, String fileName, String fileAddedTime, long fileId, String fileMime) {
        this.filePath = filePath;
        this.size = size;
        this.fileName = fileName;
        this.fileAddedTime = fileAddedTime;
        this.fileId = fileId;
        this.fileMime = fileMime;

    }

    public String getFilePath() {
        return filePath;
    }

    public String getSize() {
        return size;
    }

    public String getFileName() {
        return fileName;
    }

    public String getFileAddedTime() {
        return fileAddedTime;
    }

    public long getFileId() {
        return fileId;
    }

    public String getFileMime() {
        return fileMime;
    }
}
