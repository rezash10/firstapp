package com.example.firstapp.view.fragments;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.view.ViewGroupCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.firstapp.R;
import com.example.firstapp.databinding.FragmentFullListBinding;
import com.example.firstapp.viewmodel.FullFragmentViewModel;
import com.google.android.material.transition.MaterialSharedAxis;


public class FullListFragment extends Fragment {


    Fragment currentFragment;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        currentFragment = requireActivity().getSupportFragmentManager().findFragmentById(R.id.fragment)
                .getChildFragmentManager().getFragments().get(0);

        currentFragment.setEnterTransition(new MaterialSharedAxis(MaterialSharedAxis.Z,true)
                .setDuration(1000));

        currentFragment.setReturnTransition(new MaterialSharedAxis(MaterialSharedAxis.Z,false)
                .setDuration(1000));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        FragmentFullListBinding binding = FragmentFullListBinding.inflate(inflater);

        FullFragmentViewModel viewModel = new ViewModelProvider(this).get(FullFragmentViewModel.class);




        binding.setFullFragmentViewModel(viewModel);
        binding.setLifecycleOwner(getViewLifecycleOwner());

        ViewGroupCompat.setTransitionGroup(binding.fullViewGroup,true);

        viewModel.fileExists.observe(requireActivity(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {

                binding.pbLoadingFullList.setVisibility(View.GONE);
                if(aBoolean){

                    binding.rvCompleteDownloadList.setVisibility(View.VISIBLE);
                    binding.ivNotFoundFull.setVisibility(View.GONE);
                    binding.txNotFoundFull.setVisibility(View.GONE);

                }else{

                    binding.rvCompleteDownloadList.setVisibility(View.GONE);
                    binding.ivNotFoundFull.setVisibility(View.VISIBLE);
                    binding.txNotFoundFull.setVisibility(View.VISIBLE);

                }
            }
        });



        return binding.getRoot();
    }
}