package com.example.firstapp.view.activities;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;
import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import com.example.firstapp.R;
import com.example.firstapp.databinding.ActivityMainBinding;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;


public class MainActivity extends AppCompatActivity  {


    Toolbar toolbar;
    NavController controller;
    AlertDialog alertDialog ;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityMainBinding binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());



        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M&&Build.VERSION.SDK_INT<Build.VERSION_CODES.Q) {
            checkPremissions();
        }


        toolbar = binding.toolbar;
        setSupportActionBar(toolbar);


        controller = Navigation.findNavController(this,R.id.fragment);
        NavigationUI.setupActionBarWithNavController(this,controller);




    }



    private void checkPremissions() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ,Manifest.permission.READ_EXTERNAL_STORAGE
                    }
                    , 1);

        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 1) {
            if (grantResults.length >= 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            } else {

                MaterialAlertDialogBuilder alertBuilder = new MaterialAlertDialogBuilder(this, R.style.MaterialCustomAlertDialog)
                        .setTitle("مهم")
                        .setMessage("دسترسی به حافظه برای این برنامه ضروری میباشد")
                        .setNegativeButton("عدم دسترسی", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                finish();
                            }
                        }).setPositiveButton("فعال سازی", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        goToAppSetting();

                    }
                }).setCancelable(false);

                 alertDialog = alertBuilder.show();





            }
        }
    }

    private void goToAppSetting() {
        Intent settingIntent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        settingIntent.setData(Uri.fromParts("package", getPackageName(), null));
        startActivityForResult(settingIntent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {

            checkPremissions();
        }
    }




    @Override
    public boolean onSupportNavigateUp() {
        return controller.navigateUp()||super.onSupportNavigateUp();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if(alertDialog!=null){
            alertDialog.dismiss();
        }
    }
}